import java.util.jar._

organization in ThisBuild := "com.gu"

releaseSettings

scalaVersion in ThisBuild := "2.11.7"

crossScalaVersions := Seq("2.11.7")

publishArtifact := false

packageOptions in ThisBuild <+= (version, name) map { (v, n) =>
  Package.ManifestAttributes(
    Attributes.Name.IMPLEMENTATION_VERSION -> v,
    Attributes.Name.IMPLEMENTATION_TITLE -> n,
    Attributes.Name.IMPLEMENTATION_VENDOR -> "guardian.co.uk"
  )
}

scalacOptions in ThisBuild += "-deprecation"

publishTo := Some("uxforms-public-upload" at "s3://artifacts-public.uxforms.net")